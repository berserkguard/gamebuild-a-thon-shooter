require "Projectile"

-- Definition of the laser beam projectile class
local Asteroid = class(Actor,
    function(self, x, y)
        Actor.init(self)
        self.x = x
        self.y = y
        self.z = 0
        self.vel_x = 0
        self.vel_y = -1000
        self.vel_z = 0
        self.size = 10

        self.time = math.random(0, 99)
        self.frameRate = math.random(24, 100)
    end
)

function Asteroid:update(dt)
    self.x = self.x + self.vel_x * dt
    self.y = self.y + self.vel_y * dt
    self.size = self.size + self.vel_z * dt
    self.vel_z = self.vel_z - 9.81 * dt

    -- Remove this laser beam when it gets to far up.
    if self.y < -100 then
        self:remove()
    end
    if damageTarget(self.x, self.y, self.owner.player.pDam) then
        self:remove()
    end

    self.time = self.time + dt * self.frameRate
    if self.time >= 100 then
        self.time = self.time - 100
    end
end

function  Asteroid:draw()
    t = math.floor(self.time)
    aquad = love.graphics.newQuad((t % 10) * 128, math.floor(t / 10) * 128, 128, 128, meteorAtlas:getWidth(), meteorAtlas:getHeight())
    love.graphics.drawq(meteorAtlas, aquad, self.x, self.y)
end
---

-- Definition of the laser weapon class
local AsteroidTosser = class(Actor,
    function(self)
        Actor.init(self)
        self.cooloff = 0
    end
)

AsteroidTosser.weaponOrder = 1
AsteroidTosser.weaponName = "Asteroid++"
meteorAtlas = love.graphics.newImage("sprites.png")

local kd = love.keyboard.isDown -- A small shortcut to this crucial functino

function AsteroidTosser:update(dt)

end

function AsteroidTosser:shoot()
    self.cooloff = self.cooloff + dt
    while self.cooloff >= 0 do
        local ass = Asteroid(self.player.x, self.player.y - self.player.height / 2 - 10)
        ass.vel_x = math.random(-100, 100) + self.player.xvel
        ass.vel_y = math.random(-500, -200) + self.player.yvel
        ass.vel_z = 10
        ass.owner = self
        addActor(ass)
        ass:update(self.cooloff)
        self.cooloff = self.cooloff - self.player.pRate
    end
end

return AsteroidTosser
