--- Library Loading Code
-- Loads each lua file in a given folder using require.
-- If libTable is given, the modules' return values is inserted into it.
-- @param folder Folder to load lua files from.
-- @param libTable (Optional) A table to put the module's return values using
--   table.insert.
function loadLibrary(folder, libTable)
    local libFiles = love.filesystem.enumerate(folder)
    for _,file in ipairs(libFiles) do
        if love.filesystem.isFile(folder.."/"..file) then
            local match = file:match("(.+)[.]lua$")
            if match and match ~= "main" then
                local lib = require(folder.."/"..match)
                if libTable and lib then
                    table.insert(libTable, lib)
                end
            end
        end
    end
end

loadLibrary("core")
loadLibrary("")

-- Declare these params now. They are filled in at love.load
displayWidth = 200
arenaWidth = 0
arenaHeight = 0

-- Game Assets
local debugFont
local guiFont
local scoreFont
local playerFont
local loseFont


-- Gameplay Globals
score = 0
player = nil
weapon = nil
powerups = {}

-- Enemy Classes
enemyClasses = { }
loadLibrary("enemies", enemyClasses)


-- Weapon Classes
weaponClasses = { }
loadLibrary("weapons", weaponClasses)
table.sort(weaponClasses, function(w1, w2) return w1.weaponOrder < w2.weaponOrder end)


function love.load()
    -- Load fonts
    debugFont = love.graphics.newFont(12)
    guiFont = love.graphics.newFont("gui_font.ttf", 32)
    scoreFont = love.graphics.newFont("score_font.ttf", 18)
    playerFont = love.graphics.newFont("gui_font.ttf", 16)
    loseFont = love.graphics.newFont("score_font.ttf", 72)

    -- Load UI stuff
    uiCore = love.graphics.newImage("sprites/ui/ui.png")
    uiBorder = love.graphics.newImage("sprites/ui/border.png")
    uiGold = love.graphics.newImage("sprites/ui/gold.png")
    uiHPBarSm = love.graphics.newImage("sprites/ui/hp_bar_sm.png")
    uiHPBar = love.graphics.newImage("sprites/ui/hp_bar.png")
    uiSPBar = love.graphics.newImage("sprites/ui/sp_bar.png")
    uiP1panel = love.graphics.newImage("sprites/ui/p1_window.png")
    uiP2panel = love.graphics.newImage("sprites/ui/p2_window.png")

    -- Set the playable area geometry
    arenaWidth = love.graphics.getWidth()
    arenaHeight = love.graphics.getHeight()

    -- Add each enemy type
    for _,enemy in ipairs(enemyClasses) do
        addActor(enemy())
    end

    -- Add the player and weapon actor
    player1 = Player()
    player1.kleft = "a"
    player1.kright = "d"
    player1.kup = "w"
    player1.kdown = "s"
    player1.kshoot = "lshift"
    weapon1 = weaponClasses[1]()
    weapon1.player = player1
    player1.x = arenaWidth / 3
    player1.y = arenaHeight - 70
    player1.playerAtlas = love.graphics.newImage("dragon1.png")
    player1.playerQuads = makeAtlasQuads(player1.playerAtlas, 13)

    player2 = Player()
    player2.kleft = "kp4"
    player2.kright = "kp6"
    player2.kup = "kp8"
    player2.kdown = "kp5"
    player2.kshoot = "kpenter"
    weapon2 = weaponClasses[1]()
    weapon2.player = player2
    player2.x = arenaWidth / 3 * 2
    player2.y = arenaHeight - 72
    player2.playerAtlas = love.graphics.newImage("dragon2.png")
    player2.playerQuads = makeAtlasQuads(player2.playerAtlas, 13)

    addActor(player1)
    addActor(player2)
    addActor(weapon1)
    addActor(weapon2)

    grassGround = love.graphics.newImage("sprites/floor.png")
    trees = {
        {im = love.graphics.newImage("sprites/trees/tree1.png")},
        {im = love.graphics.newImage("sprites/trees/tree2.png")},
        {im = love.graphics.newImage("sprites/trees/tree3.png")},
        {im = love.graphics.newImage("sprites/trees/tree4.png")},
        {im = love.graphics.newImage("sprites/trees/tree5.png")},
        {im = love.graphics.newImage("sprites/trees/tree6.png")},
        {im = love.graphics.newImage("sprites/trees/tree7.png")}}
    for i, tree in pairs(trees) do
        tree.x = math.random(-20, arenaWidth)
        tree.y = math.random(arenaHeight * -.2, arenaHeight * 1.2)
    end
    powerupsG = love.graphics.newImage("sprites/powerups.png")
end

function collides(x, y, x2, y2, width2, height2)
    col = (x > x2 and x < x2 + width2 and y > y2 and y < y2 + height2)
    return col
end

grassDown = 0
function love.update(dt)
    for _,actor in ipairs(actors) do
        if actor then
            actor:update(dt)
        end
    end

    if (collides(player1.x, player1.y, player2.x, player2.y, player2.width, player2.height) or
        collides(player1.x + player1.width, player1.y, player2.x, player2.y, player2.width, player2.height) or
        collides(player1.x, player1.y + player1.height, player2.x, player2.y, player2.width, player2.height) or
        collides(player1.x + player1.width, player1.y + player1.height, player2.x, player2.y, player2.width, player2.height)) then
        if (math.abs(player1.x - player2.x) > math.abs(player1.y - player2.y)) then
            totes = math.abs(player1.xvel) + math.abs(player2.xvel)
            if (player1.x > player2.x) then
                player1.xvel = totes / 2
                player2.xvel = -totes / 2
            else
                player2.xvel = totes / 2
                player1.xvel = -totes / 2
            end
            if (math.abs(player1.xvel - player2.xvel) >= 5) then
                temp = player1.powers
                player1.powers = player2.powers
                player2.powers = temp
                player1:updatePowers()
                player2:updatePowers()
            end
        else
            totes = math.abs(player1.yvel) + math.abs(player2.yvel)
            if (player1.y > player2.y) then
                player1.yvel = totes / 2
                player2.yvel = -totes / 2
            else
                player2.yvel = totes / 2
                player1.yvel = -totes / 2
            end
            if (math.abs(player1.yvel - player2.yvel) >= 5) then
                temp = player1.powers
                player1.powers = player2.powers
                player2.powers = temp
                player1:updatePowers()
                player2:updatePowers()
            end
        end
    end

    grassDown = grassDown + dt * 60
    if (grassDown > grassGround:getHeight()) then
        grassDown = grassDown - grassGround:getHeight()
    end

    for i, tree in pairs(trees) do
        tree.y = tree.y + dt * 60
        if (tree.y > arenaHeight * 1.2) then
            tree.y = tree.y - arenaHeight * 1.4
            tree.x = math.random(-20, arenaWidth)
        end
    end

    for i, pu in pairs(powerups) do
        pu.y = pu.y + 60 * dt
        if (collides(pu.x, pu.y, player1.x - player1.width / 2, player1.y - player1.height / 2, player1.width, player1.height)) then
            player1:getPower(pu.index)
            table.remove(powerups, i)
        elseif (collides(pu.x, pu.y, player2.x - player1.width / 2, player2.y - player1.height / 2, player2.width, player2.height)) then
            player2:getPower(pu.index)
            table.remove(powerups, i)
        end
    end
end

function love.draw()
    -- Draw all the actors
    love.graphics.push()
    --love.graphics.translate(displayWidth, 0)
    gquad = love.graphics.newQuad(0, 0, grassGround:getWidth(), grassGround:getHeight(), grassGround:getWidth(), grassGround:getHeight())
    love.graphics.drawq(grassGround, gquad, 0, grassDown + 200)
    love.graphics.drawq(grassGround, gquad, 0, grassDown - grassGround:getHeight() + 200)
    love.graphics.drawq(grassGround, gquad, 0, grassDown - grassGround:getHeight() * 2 + 200)

    for i, tree in pairs(trees) do
        tquad = love.graphics.newQuad(0, 0, tree.im:getWidth(), tree.im:getHeight(), tree.im:getWidth(), tree.im:getHeight())
        love.graphics.drawq(tree.im, tquad, tree.x, tree.y)
    end

    for i, pu in pairs(powerups) do
        pquad = love.graphics.newQuad((pu.index - 1) * 46, 0, 46, 46, powerupsG:getWidth(), powerupsG:getHeight())
        love.graphics.drawq(powerupsG, pquad, pu.x, pu.y, 0, .5, .5)
    end

    for i = 0, 2 do
        for _,actor in ipairs(actors) do
            if (actor) and actor.z == i then
                actor:draw(dt)
            end
        end
    end
    love.graphics.pop()

    -- Render the UI
    renderUI()

    -- Draw some debug stuff. Disable by replacing the condition with false, or set it to true to
    -- enable it.
    if false then
        love.graphics.setFont(debugFont)
        local debugFontHeight = debugFont:getHeight()
        love.graphics.print("Memory: " .. math.ceil(collectgarbage("count")) .. "KB", 0, debugFontHeight * 0)
        love.graphics.print("Actors: " .. (#actors - #actors.free),                   0, debugFontHeight * 1)
        love.graphics.print("Free Actors: " .. (#actors.free),                        0, debugFontHeight * 2)
        love.graphics.print("Targets: " .. (#targets),                                0, debugFontHeight * 3)
    end
end

function renderUI()
    if (player1.health <= 0 and player2.health <= 0) then
        love.graphics.setFont(loseFont)
        love.graphics.print("YOU LOSE", 100, 250)
    end

    -- Draw Player 1 BG panel
    uiquad = love.graphics.newQuad(0, 0, uiP1panel:getWidth(), uiP1panel:getHeight(), uiP1panel:getWidth(), uiP1panel:getHeight())
    love.graphics.drawq(uiP1panel, uiquad, 3, 488)
    -- Draw the HP Bar
    hpRatio = player1.health / player1.maxHealth
    barX = uiHPBar:getWidth() / 2 - hpRatio * uiHPBar:getWidth() / 2
    uiquad = love.graphics.newQuad(barX, 0, uiHPBar:getWidth() / 2, uiHPBar:getHeight(), uiHPBar:getWidth(), uiHPBar:getHeight())
    love.graphics.drawq(uiHPBar, uiquad, 75, 509)

    -- Draw the SP Bar
    spRatio = player1.SP / player1.maxSP
    barX = uiSPBar:getWidth() / 2 - spRatio * uiSPBar:getWidth() / 2
    uiquad = love.graphics.newQuad(barX, 0, uiSPBar:getWidth() / 2, uiSPBar:getHeight(), uiSPBar:getWidth(), uiSPBar:getHeight())
    love.graphics.drawq(uiSPBar, uiquad, 75, 539)

    -- Draw the UI thing
    uiquad = love.graphics.newQuad(0, 0, uiCore:getWidth(), uiCore:getHeight(), uiCore:getWidth(), uiCore:getHeight())
    love.graphics.drawq(uiCore, uiquad, 3, 488)

    -- Draw power ups
    for i, powerup in pairs(player1.powers) do
        pquad = love.graphics.newQuad((powerup - 1) * 46, 0, 46, 46, powerupsG:getWidth(), powerupsG:getHeight())
        love.graphics.drawq(powerupsG, pquad, 136 + 29 * i, 563, 0, 20/46, 20/46)
    end




    -- Draw Player 2 BG panel
    uiquad = love.graphics.newQuad(0, 0, uiP2panel:getWidth(), uiP2panel:getHeight(), uiP2panel:getWidth(), uiP2panel:getHeight())
    love.graphics.drawq(uiP2panel, uiquad, 570, 488)
    
    -- Draw the HP Bar
    hpRatio = player2.health / player2.maxHealth
    barX = uiHPBar:getWidth() / 2 - hpRatio * uiHPBar:getWidth() / 2
    uiquad = love.graphics.newQuad(barX, 0, uiHPBar:getWidth() / 2, uiHPBar:getHeight(), uiHPBar:getWidth(), uiHPBar:getHeight())
    love.graphics.drawq(uiHPBar, uiquad, 642, 509)

    -- Draw the SP Bar
    spRatio = player2.SP / player2.maxSP
    barX = uiSPBar:getWidth() / 2 - spRatio * uiSPBar:getWidth() / 2
    uiquad = love.graphics.newQuad(barX, 0, uiSPBar:getWidth() / 2, uiSPBar:getHeight(), uiSPBar:getWidth(), uiSPBar:getHeight())
    love.graphics.drawq(uiSPBar, uiquad, 642, 539)

    -- Draw the UI thing
    uiquad = love.graphics.newQuad(0, 0, uiCore:getWidth(), uiCore:getHeight(), uiCore:getWidth(), uiCore:getHeight())
    love.graphics.drawq(uiCore, uiquad, 570, 488)

    -- Draw power ups
    for i, powerup in pairs(player2.powers) do
        pquad = love.graphics.newQuad((powerup - 1) * 46, 0, 46, 46, powerupsG:getWidth(), powerupsG:getHeight())
        love.graphics.drawq(powerupsG, pquad, 703 + 29 * i, 563, 0, 20/46, 20/46)
    end




    -- Draw the score (gold)
    uiquad = love.graphics.newQuad(0, 0, uiGold:getWidth(), uiGold:getHeight(), uiGold:getWidth(), uiGold:getHeight())
    love.graphics.drawq(uiGold, uiquad, 6, 6)
    love.graphics.setFont(scoreFont)
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.print(numberToString(score, 8), 47, 18)

    love.graphics.setColor(255, 255, 255, 255)
end

