require "Projectile"

-- Definition of the laser beam projectile class
local Asteroid = class(Actor,
    function(self, x, y)
        Actor.init(self)
        self.x = x
        self.y = y
        self.z = 0
        self.vel_x = 0
        self.vel_y = -1000
        self.vel_z = 1
        self.size = 20

        self.time = math.random(0, 99)
        self.frameRate = math.random(24, 100)
    end
)

function hurtPlayer(player, vel_x, vel_y)
    if (player.shielded) then
        player:unshield()
    else
        player.health = player.health - 1
        player.xvel = player.xvel + vel_x / 50
        player.yvel = player.yvel + vel_y / 50
        if (player.powers[2] > 0) then
            player.powers[2] = 0
        elseif (player.powers[1] > 0) then
            player.powers[1] = 0
        end
    end
end

function Asteroid:update(dt)
    self.x = self.x + self.vel_x * dt
    self.y = self.y + self.vel_y * dt
    self.size = self.size + self.vel_z * dt
    self.vel_z = self.vel_z - 200 * dt

    if self.y > arenaHeight + 100 or self.size < 18 then
        self:remove()
    else
        self.time = self.time + dt * self.frameRate
        if self.time >= 100 then
            self.time = self.time - 100
        end

        siz = self.size / 100 * 64
        if (collides(self.x - siz, self.y - siz, player1.x - player1.width / 2, player1.y - player1.height / 2, player1.width / 2, player1.height / 2)) then
            hurtPlayer(player1, self.vel_x, self.vel_y)
            self:remove()
        elseif (collides(self.x - siz, self.y - siz, player2.x - player2.width / 2, player2.y - player2.height / 2, player2.width / 2, player2.height / 2)) then
            hurtPlayer(player2, self.vel_x, self.vel_y)
            self:remove()
        end
    end
end

function  Asteroid:draw()
    t = math.floor(self.time)
    aquad = love.graphics.newQuad((t % 10) * 128, math.floor(t / 10) * 128, 128, 128, meteorAtlas:getWidth(), meteorAtlas:getHeight())
    siz = self.size / 100
    love.graphics.drawq(meteorAtlas, aquad, self.x - siz * 64, self.y - siz * 64, 0, siz, siz)
end
---

-- Definition of the laser weapon class
AsteroidTosser = class(Actor,
    function(self)
        Actor.init(self)
        self.cooloff = 0
    end
)

AsteroidTosser.weaponOrder = 1
AsteroidTosser.weaponName = "Asteroid++"
meteorAtlas = love.graphics.newImage("sprites.png")

local kd = love.keyboard.isDown -- A small shortcut to this crucial functino

function AsteroidTosser:update(dt)

end

function AsteroidTosser:shoot(x, y, rot)
    local ass = Asteroid(x, y)
    ass.vel_x = 400 * math.cos(rot + math.pi / 2)
    ass.vel_y = 400 * math.sin(rot + math.pi / 2)
    ass.vel_z = 100
    ass.owner = self
    addActor(ass)
    ass:update(self.cooloff)
end

return AsteroidTosser
