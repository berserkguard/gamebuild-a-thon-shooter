--0 = none, 1 = rate, 2 = damage, 3 = speed, 4 = mega

Player = class(Actor,
    function(self)
        Actor.init(self)
        self.x = 0
        self.y = 0
        self.xvel = 0
        self.yvel = 0
        self.width = 64
        self.height = 120
        self.moveSpeed = 400
        self.targetTiltAngle = 0
        self.tiltAngle = 0
        self.z = 2
        self.rot = 0
        self.shielded = false

        self.maxHealth = 4
        self.health = 4
        self.maxSP = 100
        self.SP = 100
        self.SPRegenTime = 0.0
        self.SPRegenRate = 0.1

        self.kleft = "left"
        self.kright = "right"
        self.kup = "up"
        self.kdown = "down"
        self.kspace = " "

        self.powers = {0, 0}

        self.pAccel = 35
        self.pDam = 1
        self.pRate = .05
    end
)

function decel(vel, accel, nav)
    if (vel > 0) then vel = vel - accel end
    if (vel < 0) then vel = vel + accel end
    if (nav == false and math.abs(vel) < accel) then vel = 0 end
    return vel
end

function Player:unshield()
    if (self.powers[1] == 6) then
        self.powers[1] = self.powers[2]
        if (self.powers[1] ~= 6) then self.shielded = false end
    elseif (self.powers[2] == 6) then
        self.powers[2] = 0
    end
end

function Player:updatePowers()
    self.pAccel = 35
    self.pDam = 1
    self.pRate = .05
    self.SPRegenRate = 0.3
    self.shielded = false
    for i = 1, 2 do
        if (self.powers[i] == 1) then
            self.pRate = self.pRate - .015
        elseif (self.powers[i] == 2) then
            self.pDam = self.pDam + .3
        elseif (self.powers[i] == 3) then
            self.pAccel = self.pAccel + 10
        elseif (self.powers[i] == 5) then
            self.SPRegenRate = self.SPRegenRate - .1
            self.pDam = self.pDam + .5
            self.pRate = self.pRate + .02
        elseif (self.powers[i] == 6) then
            self.shielded = true
        end
    end
end

function Player:getPower(powerI)
    if (powerI == 4) then
        self.SP = self.SP + 50
        if (self.SP > self.maxSP) then self.SP = self.maxSP end
    else
        self.powers[2] = self.powers[1]
        self.powers[1] = powerI

        self:updatePowers()
    end
end

local kd = love.keyboard.isDown
function Player:update(dt)
    navx = false
    navy = false

    -- SP Regen stuff
    self.SPRegenTime = self.SPRegenTime + dt
    while(self.SPRegenTime > self.SPRegenRate) do
        self.SPRegenTime = self.SPRegenTime - self.SPRegenRate
        if(self.SP < self.maxSP) then
            self.SP = self.SP + 1
        end
    end

    if (self.health > 0) then
        navx = false
        navy = false
        if kd(self.kleft) then
            self.xvel = self.xvel - self.pAccel * dt
            self.targetTiltAngle = -45
            navx = true
        elseif kd(self.kright) then
            self.xvel = self.xvel + self.pAccel * dt
            self.targetTiltAngle = 45
            navx = true
        else
            self.targetTiltAngle = 0
        end
        if kd(self.kup) then
            self.yvel = self.yvel - self.pAccel * dt
            navy = true
        elseif kd(self.kdown) then
            self.yvel = self.yvel + self.pAccel * dt
            navy = true
        end
    end

    self.x = self.x + self.xvel
    self.xvel = decel(self.xvel, 20 * dt, navx)
    self.y = self.y + self.yvel
    self.yvel = decel(self.yvel, 20 * dt, navy)

    if (self.health <= 0) then self.yvel = self.yvel + 50 * dt end

    if (self.health > 0) then
        if self.x - self.width / 2 < 0 then
            self.xvel = -self.xvel
            self.x = self.width / 2
        elseif self.x + self.width / 2 > arenaWidth then
            self.xvel = -self.xvel
            self.x = arenaWidth - self.width / 2
        end

        if self.y - self.height / 2 < 0 then
            self.yvel = -self.yvel
            self.y = self.height / 2
        elseif self.y + self.height / 2 > arenaHeight then
            self.yvel = -self.yvel
            self.y = arenaHeight - self.height / 2
        end
    end

    if (self.xvel > 10) then self.xvel = 10
    elseif (self.xvel < -10) then self.xvel = -10 end
    if (self.yvel > 10) then self.yvel = 10
    elseif (self.yvel < -10) then self.yvel = -10 end

    -- Tilt Angle Calc
    local tiltDelta = self.targetTiltAngle - self.tiltAngle
    if math.abs(tiltDelta) < dt * 200 then -- To prevent jiggle around the 0 angle.
        self.tiltAngle = self.targetTiltAngle
    else
        self.tiltAngle = self.tiltAngle + (tiltDelta / math.abs(tiltDelta)) * dt * 200
        self.tiltAngle = math.min(math.max(self.tiltAngle, -45), 45)
    end
    if (math.abs(self.rot - self.xvel / 50) < .0085) then self.rot = self.xvel / 50
    elseif (self.rot < self.xvel / 50) then
        self.rot = self.rot + 0.008
    else
        self.rot = self.rot - 0.008
    end
end

function Player:draw(dt)
    wid = self.playerAtlas:getWidth() / 13
    hei = self.playerAtlas:getHeight() / 7
    love.graphics.setColor(0, 0, 0, 127)
    love.graphics.drawq(self.playerAtlas, self.playerQuads[math.floor(self.tiltAngle) + 46], self.x - 80, self.y - 50, self.rot)
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.drawq(self.playerAtlas, self.playerQuads[math.floor(self.tiltAngle) + 46], self.x - wid / 2, self.y - hei / 2, self.rot)
    if (self.shielded) then
        love.graphics.setColor(0, 255, 255, 127)
        love.graphics.circle("fill", self.x, self.y, self.height / 2, 20)
    end
    love.graphics.setColor(255, 255, 255, 255)
end
